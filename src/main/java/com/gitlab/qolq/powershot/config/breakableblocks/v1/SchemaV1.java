package com.gitlab.qolq.powershot.config.breakableblocks.v1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.gitlab.qolq.powershot.config.breakableblocks.BreakableBlocksConfig;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import net.minecraft.tags.TagKey;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;

import static com.gitlab.qolq.powershot.Powershot.log;

public final class SchemaV1
{
    private static final String BLOCKS_KEY = "blocks";
    private static final String TAGS_KEY = "tags";
    private static final String POWER_REQ_KEY = "power_req";
    private static final String POWER_RED_KEY = "power_red";

    public static BreakableBlocksConfig read(JsonArray json)
    {
        Map<BlockState, BreakableBlocksConfig.Entry> statesToEntries = new HashMap<>();
        Map<TagKey<Block>, BreakableBlocksConfig.Entry> tagsToEntries = new LinkedHashMap<>();

        for (JsonElement entry : json)
        {
            if (entry.isJsonObject())
            {
                readEntry(entry.getAsJsonObject(), statesToEntries, tagsToEntries);
            }
            else
            {
                log.debug("Invalid breakable blocks entry (not an object): {}", entry);
            }
        }

        return new BreakableBlocksConfig(
            statesToEntries.isEmpty() ? Collections.emptyMap() : statesToEntries,
            tagsToEntries.isEmpty() ? Collections.emptyMap() : tagsToEntries
        );
    }

    private static void readEntry(JsonObject json,
                                  Map<BlockState, BreakableBlocksConfig.Entry> statesToEntries,
                                  Map<TagKey<Block>, BreakableBlocksConfig.Entry> tagsToEntries)
    {
        Collection<BlockState> states = new ArrayList<>();
        Collection<TagKey<Block>> tags = new ArrayList<>();
        float powerRequirement = BreakableBlocksConfig.Entry.DEFAULT_POWER_REQUIREMENT;
        float powerReduction = BreakableBlocksConfig.Entry.DEFAULT_POWER_REDUCTION;

        for (Map.Entry<String, JsonElement> entry : json.entrySet())
        {
            try
            {
                switch (entry.getKey())
                {
                    case BLOCKS_KEY -> BlockEntries.read(entry.getValue(), states);
                    case TAGS_KEY -> TagEntries.read(entry.getValue(), tags);
                    case POWER_REQ_KEY -> powerRequirement = GsonHelper.convertToFloat(entry.getValue(), POWER_REQ_KEY);
                    case POWER_RED_KEY -> powerReduction = GsonHelper.convertToFloat(entry.getValue(), POWER_RED_KEY);
                    default -> log.debug("Ignoring unrecognized breakable blocks entry property: {}", entry.getKey());
                }
            }
            catch (JsonParseException e)
            {
                log.debug("Failed to read breakable blocks entry property: " + entry.getKey(), e);
            }
        }

        if (states.isEmpty() && tags.isEmpty())
        {
            log.debug("Ignoring invalid breakable blocks entry (no blocks or tags specified): {}", json);
            return;
        }

        BreakableBlocksConfig.Entry entry = new BreakableBlocksConfig.Entry(powerRequirement, powerReduction);

        for (BlockState state : states)
        {
            BreakableBlocksConfig.Entry replaced = statesToEntries.put(state, entry);
            if (replaced != null)
                log.debug("Replaced breakable blocks block state entry: {} ({} -> {})", state, replaced, entry);
        }

        for (TagKey<Block> tag : tags)
        {
            BreakableBlocksConfig.Entry replaced = tagsToEntries.put(tag, entry);
            if (replaced != null)
                log.debug("Replaced breakable blocks tag entry: {} ({} -> {})", tag, replaced, entry);
        }
    }
}
