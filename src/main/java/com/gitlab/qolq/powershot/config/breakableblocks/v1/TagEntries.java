package com.gitlab.qolq.powershot.config.breakableblocks.v1;

import java.util.Collection;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import net.minecraft.ResourceLocationException;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.level.block.Block;

import static com.gitlab.qolq.powershot.Powershot.log;

public final class TagEntries
{
    public static void read(JsonElement json, Collection<TagKey<Block>> tags)
    {
        if (json.isJsonArray())
        {
            readArrayTagEntry(json.getAsJsonArray(), tags);
        }
        else if (json.isJsonPrimitive() && ((JsonPrimitive)json).isString())
        {
            readStringTagEntry(json.getAsJsonPrimitive(), tags);
        }
        else
        {
            log.debug("Invalid tags entry (was not a string nor an array): {}", json);
        }
    }

    private static void readArrayTagEntry(JsonArray json, Collection<TagKey<Block>> tags)
    {
        for (JsonElement jsonTag : json)
        {
            try
            {
                if (jsonTag.isJsonPrimitive() && ((JsonPrimitive)jsonTag).isString())
                {
                    readStringTagEntry(jsonTag.getAsJsonPrimitive(), tags);
                }
                else
                {
                    log.debug("Invalid tag entry (was not a string): {}", jsonTag);
                }
            }
            catch (JsonParseException e)
            {
                log.debug("Failed to read tag entry", e);
            }
        }
    }

    private static void readStringTagEntry(JsonPrimitive json, Collection<TagKey<Block>> tags)
    {
        try
        {
            tags.add(TagKey.create(Registries.BLOCK, new ResourceLocation(GsonHelper.convertToString(json, "tag"))));
        }
        catch (ResourceLocationException e)
        {
            log.debug("Invalid tag entry (invalid id): {}", json);
        }
    }
}
