package com.gitlab.qolq.changelogsnippet

import org.gradle.api.Plugin
import org.gradle.api.Project

@SuppressWarnings("unused")
class ChangelogSnippetPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.tasks.register('changelogSnippet', ChangelogSnippetTask) {
            group = 'Changelog Snippet'
            description = 'Creates a changelog containing a snippet from a source changelog.'
        }
    }
}
