package com.gitlab.qolq.powershot.config.powercalc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

import com.gitlab.qolq.powershot.config.powercalc.v1.SchemaV1;
import com.gitlab.qolq.powershot.config.powercalc.v2.SchemaV2;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import it.unimi.dsi.fastutil.objects.Object2FloatMap;
import it.unimi.dsi.fastutil.objects.Object2FloatMaps;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.GsonHelper;
import org.apache.commons.io.FileUtils;

import static com.gitlab.qolq.powershot.Powershot.log;

public record PowerCalculationConfig(float basePower, Object2FloatMap<ResourceLocation> enchantmentToMultiplier)
{
    public static final float DEFAULT_BASE_POWER = 1.0f;
    public static final String SCHEMA_VERSION_KEY = "schema_version";

    private static final String DEFAULT_CONFIG_PATH = "/data/powershot/default_power_calculation.json";

    public PowerCalculationConfig(float basePower, Object2FloatMap<ResourceLocation> enchantmentToMultiplier)
    {
        this.basePower = basePower;
        this.enchantmentToMultiplier = Object2FloatMaps.unmodifiable(enchantmentToMultiplier);
    }

    public PowerCalculationConfig()
    {
        this(DEFAULT_BASE_POWER, Object2FloatMaps.emptyMap());
    }

    public static PowerCalculationConfig from(File file)
    {
        if (!file.exists())
        {
            try (InputStream stream = PowerCalculationConfig.class.getResourceAsStream(DEFAULT_CONFIG_PATH))
            {
                if (stream == null)
                {
                    log.debug("Failed to write default power calculation config file (file not found)");
                    return new PowerCalculationConfig();
                }
                FileUtils.copyInputStreamToFile(stream, file);
            }
            catch (IOException e)
            {
                log.warn("Failed to write default power calculation config file", e);
                return new PowerCalculationConfig();
            }
        }

        try (Reader reader = new BufferedReader(new FileReader(file)))
        {
            Gson gson = new GsonBuilder().disableHtmlEscaping().create();
            JsonObject json = gson.fromJson(reader, JsonObject.class);

            if (!json.has(SCHEMA_VERSION_KEY))
            {
                log.debug("Power calculation config file has no schema version");
                return SchemaV1.read(json); // first version had no `schema_version` property
            }

            //noinspection EnhancedSwitchMigration
            switch (GsonHelper.getAsInt(json, SCHEMA_VERSION_KEY, Integer.MAX_VALUE))
            {
                case 1:
                    log.info("Power calculation config file is in an outdated format. Please consider updating it");
                    return SchemaV1.read(json);
                case 2:
                    return SchemaV2.read(json);
                default:
                    log.debug("Unsupported power calculation config schema version: {}", json.get(SCHEMA_VERSION_KEY));
                    return SchemaV2.read(json);
            }
        }
        catch (IOException | JsonParseException e)
        {
            log.warn("Failed to read file " + file, e);
            return new PowerCalculationConfig();
        }
    }
}
