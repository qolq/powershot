package com.gitlab.qolq.powershot.mixin;

import com.gitlab.qolq.powershot.Powerable;
import com.gitlab.qolq.powershot.Powershot;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.projectile.AbstractArrow;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.BlockHitResult;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(AbstractArrow.class)
@SuppressWarnings("unused")
public abstract class AbstractArrowMixin extends Entity implements Powerable
{
    @Unique
    private static final String POWER_KEY = Powershot.MOD_ID + ":power";

    @Unique
    private float power = 0.0f;

    public AbstractArrowMixin(EntityType<?> type, Level level)
    {
        super(type, level);
    }

    @Unique
    @Override
    public float powershot$getPower()
    {
        return power;
    }

    @Unique
    @Override
    public void powershot$setPower(float power)
    {
        this.power = power;
    }

    @Inject(method = "shoot(DDDFF)V", at = @At("HEAD"))
    private void setPower(double x, double y, double z, float speed, float inaccuracy, CallbackInfo info)
    {
        //noinspection resource
        if (!level().isClientSide)
            //noinspection ConstantConditions
            powershot$setPower(Powershot.calculatePower((AbstractArrow)(Object)this, speed));
    }

    @Redirect(
        method = "tick()V",
        slice = @Slice(
            from = @At(
                value = "FIELD",
                target = "Lnet/minecraft/world/entity/projectile/AbstractArrow;inGroundTime:I",
                opcode = Opcodes.PUTFIELD,
                ordinal = 1
            ),
            to = @At(
                value = "INVOKE",
                target = "Lnet/minecraft/world/phys/HitResult;getLocation()Lnet/minecraft/world/phys/Vec3;"
            )
        ),
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/world/level/Level;clip(Lnet/minecraft/world/level/ClipContext;)" +
                     "Lnet/minecraft/world/phys/BlockHitResult;",
            ordinal = 0
        )
    )
    private BlockHitResult clip(Level level, ClipContext context)
    {
        //noinspection ConstantConditions
        return Powershot.clip(level, context, (AbstractArrow)(Object)this);
    }

    @Inject(method = "addAdditionalSaveData(Lnet/minecraft/nbt/CompoundTag;)V", at = @At("HEAD"))
    private void writeData(CompoundTag tag, CallbackInfo info)
    {
        tag.putFloat(POWER_KEY, powershot$getPower());
    }

    @Inject(method = "readAdditionalSaveData(Lnet/minecraft/nbt/CompoundTag;)V", at = @At("HEAD"))
    private void readData(CompoundTag tag, CallbackInfo info)
    {
        powershot$setPower(tag.getFloat(POWER_KEY));
    }
}
