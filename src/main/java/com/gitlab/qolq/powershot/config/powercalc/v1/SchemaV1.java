package com.gitlab.qolq.powershot.config.powercalc.v1;

import java.util.Map;

import com.gitlab.qolq.powershot.config.powercalc.PowerCalculationConfig;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import it.unimi.dsi.fastutil.objects.Object2FloatMap;
import it.unimi.dsi.fastutil.objects.Object2FloatMaps;
import it.unimi.dsi.fastutil.objects.Object2FloatOpenHashMap;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.item.enchantment.Enchantments;

import static com.gitlab.qolq.powershot.Powershot.log;
import static com.gitlab.qolq.powershot.config.powercalc.PowerCalculationConfig.SCHEMA_VERSION_KEY;

public final class SchemaV1
{
    private static final String BASE_POWER_KEY = "base_power";
    private static final String POWER_MULTIPLIER_KEY = "power_multiplier";
    private static final String PIERCING_MULTIPLIER_KEY = "piercing_multiplier";
    private static final String PUNCH_MULTIPLIER_KEY = "punch_multiplier";
    private static final String KNOCKBACK_MULTIPLIER_KEY = "knockback_multiplier";

    public static PowerCalculationConfig read(JsonObject json)
    {
        float basePower = PowerCalculationConfig.DEFAULT_BASE_POWER;
        Object2FloatMap<ResourceLocation> enchantmentToMultiplier = new Object2FloatOpenHashMap<>();

        for (Map.Entry<String, JsonElement> entry : json.entrySet())
        {
            try
            {
                switch (entry.getKey())
                {
                    case SCHEMA_VERSION_KEY -> {}
                    case BASE_POWER_KEY -> basePower = GsonHelper.convertToFloat(entry.getValue(), BASE_POWER_KEY);
                    case POWER_MULTIPLIER_KEY -> enchantmentToMultiplier.put(
                        BuiltInRegistries.ENCHANTMENT.getKey(Enchantments.POWER_ARROWS),
                        GsonHelper.convertToFloat(entry.getValue(), POWER_MULTIPLIER_KEY)
                    );
                    case PIERCING_MULTIPLIER_KEY -> enchantmentToMultiplier.put(
                        BuiltInRegistries.ENCHANTMENT.getKey(Enchantments.PIERCING),
                        GsonHelper.convertToFloat(entry.getValue(), PIERCING_MULTIPLIER_KEY)
                    );
                    case PUNCH_MULTIPLIER_KEY -> enchantmentToMultiplier.put(
                        BuiltInRegistries.ENCHANTMENT.getKey(Enchantments.PUNCH_ARROWS),
                        GsonHelper.convertToFloat(entry.getValue(), PUNCH_MULTIPLIER_KEY)
                    );
                    case KNOCKBACK_MULTIPLIER_KEY -> enchantmentToMultiplier.put(
                        BuiltInRegistries.ENCHANTMENT.getKey(Enchantments.KNOCKBACK),
                        GsonHelper.convertToFloat(entry.getValue(), KNOCKBACK_MULTIPLIER_KEY)
                    );
                    default -> log.debug("Ignoring unrecognized power calculation config property: {}", entry.getKey());
                }
            }
            catch (JsonParseException e)
            {
                log.debug("Failed to read power calculation config property: " + entry.getKey(), e);
            }
        }

        return new PowerCalculationConfig(
            basePower,
            enchantmentToMultiplier.isEmpty() ? Object2FloatMaps.emptyMap() : enchantmentToMultiplier
        );
    }
}
