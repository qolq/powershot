package com.gitlab.qolq.powershot.config.breakableblocks;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.gitlab.qolq.powershot.config.breakableblocks.v1.SchemaV1;
import com.gitlab.qolq.powershot.config.breakableblocks.v2.SchemaV2;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderSet;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.tags.TagKey;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import org.apache.commons.io.FileUtils;

import static com.gitlab.qolq.powershot.Powershot.log;

public final class BreakableBlocksConfig
{
    public static final String SCHEMA_VERSION_KEY = "schema_version";

    private static final String DEFAULT_CONFIG_PATH = "/data/powershot/default_breakable_blocks.json";

    private final Map<BlockState, Entry> statesToEntries;
    private final Map<TagKey<Block>, Entry> tagsToEntries;

    private volatile Map<BlockState, Entry> merged;

    public BreakableBlocksConfig(Map<BlockState, Entry> statesToEntries, Map<TagKey<Block>, Entry> tagsToEntries)
    {
        this.statesToEntries = Collections.unmodifiableMap(statesToEntries);
        this.tagsToEntries = Collections.unmodifiableMap(tagsToEntries);
    }

    public BreakableBlocksConfig()
    {
        this(Collections.emptyMap(), Collections.emptyMap());
    }

    public Entry get(BlockState key)
    {
        return merged.get(key);
    }

    public synchronized void reloadTags()
    {
        Map<BlockState, Entry> map = new HashMap<>();

        for (Map.Entry<TagKey<Block>, Entry> entry : tagsToEntries.entrySet())
        {
            Optional<HolderSet.Named<Block>> tag = BuiltInRegistries.BLOCK.getTag(entry.getKey());

            if (tag.isEmpty())
                continue;

            for (Holder<Block> blockHolder : tag.get())
                for (BlockState state : blockHolder.value().getStateDefinition().getPossibleStates())
                    map.put(state, entry.getValue());
        }

        map.putAll(statesToEntries);
        merged = Collections.unmodifiableMap(map);
    }

    public static BreakableBlocksConfig from(File file)
    {
        if (!file.exists())
        {
            try (InputStream stream = BreakableBlocksConfig.class.getResourceAsStream(DEFAULT_CONFIG_PATH))
            {
                if (stream == null)
                {
                    log.debug("Failed to write default breakable blocks config file (file not found)");
                    return new BreakableBlocksConfig();
                }
                FileUtils.copyInputStreamToFile(stream, file);
            }
            catch (IOException e)
            {
                log.warn("Failed to write default breakable blocks config file", e);
                return new BreakableBlocksConfig();
            }
        }

        Gson gson = new GsonBuilder().disableHtmlEscaping().create();

        try (Reader reader = new BufferedReader(new FileReader(file)))
        {
            JsonObject json = gson.fromJson(reader, JsonObject.class);

            //noinspection EnhancedSwitchMigration
            switch (GsonHelper.getAsInt(json, SCHEMA_VERSION_KEY, Integer.MAX_VALUE))
            {
                case 1:
                    log.debug("Ignoring breakable blocks config with schema v1 containing an object");
                    return new BreakableBlocksConfig();
                case 2:
                    return SchemaV2.read(json);
                default:
                    log.debug("Unsupported breakable blocks config schema version: {}", json.get(SCHEMA_VERSION_KEY));
                    return SchemaV2.read(json);
            }
        }
        catch (JsonSyntaxException e) // old schema's top-level value is an array
        {
            try (Reader reader = new BufferedReader(new FileReader(file)))
            {
                JsonArray json = gson.fromJson(reader, JsonArray.class);
                log.info("Breakable blocks config file is in an outdated format. Please consider updating it");
                return SchemaV1.read(json);
            }
            catch (IOException | JsonParseException e2)
            {
                e2.addSuppressed(e);
                log.warn("Failed to read file " + file, e2);
                return new BreakableBlocksConfig();
            }
        }
        catch (IOException | JsonParseException e)
        {
            log.warn("Failed to read file " + file, e);
            return new BreakableBlocksConfig();
        }
    }

    public record Entry(float powerRequirement, float powerReduction)
    {
        public static final float DEFAULT_POWER_REQUIREMENT = 1.5f;
        public static final float DEFAULT_POWER_REDUCTION = 1.5f;

        public Entry(float powerRequirement, float powerReduction)
        {
            this.powerRequirement = Float.isNaN(powerRequirement) ? DEFAULT_POWER_REQUIREMENT : powerRequirement;
            this.powerReduction = Float.isNaN(powerReduction) ? DEFAULT_POWER_REDUCTION : powerReduction;
        }

        @Override
        public String toString()
        {
            return String.format("[%.2f,%.2f]", powerRequirement, powerReduction);
        }
    }
}
