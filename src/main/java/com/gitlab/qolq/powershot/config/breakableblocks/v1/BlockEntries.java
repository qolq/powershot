package com.gitlab.qolq.powershot.config.breakableblocks.v1;

import java.util.Collection;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import net.minecraft.ResourceLocationException;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.Property;

import static com.gitlab.qolq.powershot.Powershot.log;

public final class BlockEntries
{
    private static final String BLOCK_ID_KEY = "id";
    private static final String BLOCK_STATES_KEY = "states";

    private static final String NORMAL_BLOCK_STATE_ENTRY = "normal";

    public static void read(JsonElement json, Collection<BlockState> states)
    {
        if (json.isJsonArray())
        {
            readArrayBlockEntry(json.getAsJsonArray(), states);
        }
        else if (json.isJsonObject())
        {
            readObjectBlockEntry(json.getAsJsonObject(), states);
        }
        else if (json.isJsonPrimitive() && ((JsonPrimitive)json).isString())
        {
            readStringBlockEntry(json.getAsJsonPrimitive(), states);
        }
        else
        {
            log.debug("Invalid blocks entry (was not a string, an object, nor an array): {}", json);
        }
    }

    private static void readArrayBlockEntry(JsonArray json, Collection<BlockState> states)
    {
        for (JsonElement jsonBlock : json)
        {
            try
            {
                if (jsonBlock.isJsonObject())
                {
                    readObjectBlockEntry(jsonBlock.getAsJsonObject(), states);
                }
                else if (jsonBlock.isJsonPrimitive() && ((JsonPrimitive)jsonBlock).isString())
                {
                    readStringBlockEntry(jsonBlock.getAsJsonPrimitive(), states);
                }
                else
                {
                    log.debug("Invalid block entry (was neither a string nor an object): {}", jsonBlock);
                }
            }
            catch (JsonParseException e)
            {
                log.debug("Failed to read block entry", e);
            }
        }
    }

    private static void readObjectBlockEntry(JsonObject json, Collection<BlockState> states)
    {
        JsonElement jsonId = json.get(BLOCK_ID_KEY);

        if (jsonId == null)
        {
            log.debug("Invalid block entry (no block id specified): {}", json);
            return;
        }

        ResourceLocation id;

        try
        {
            id = new ResourceLocation(GsonHelper.convertToString(jsonId, "id"));
        }
        catch (ResourceLocationException e)
        {
            log.debug("Invalid block entry (invalid id): {}", json);
            return;
        }

        if (!BuiltInRegistries.BLOCK.containsKey(id))
        {
            log.debug("Invalid block entry (not a registered block): {}", id);
            return;
        }

        Block block = BuiltInRegistries.BLOCK.get(id);
        JsonElement jsonStates = json.get(BLOCK_STATES_KEY);

        if (jsonStates == null)
        {
            states.addAll(block.getStateDefinition().getPossibleStates());
        }
        else
        {
            StateDefinition<Block, BlockState> definition = block.getStateDefinition();

            for (JsonElement jsonState : GsonHelper.convertToJsonArray(jsonStates, "block states entry"))
            {
                try
                {
                    String stringState = GsonHelper.convertToString(jsonState, "block state entry");
                    BlockState state = readBlockStateEntry(stringState, definition, id);
                    if (state != null)
                        states.add(state);
                }
                catch (JsonParseException e)
                {
                    log.debug("Failed to read '" + id + "' state entry: " + jsonState, e);
                }
            }
        }
    }

    private static void readStringBlockEntry(JsonPrimitive json, Collection<BlockState> states)
    {
        try
        {
            ResourceLocation id = new ResourceLocation(GsonHelper.convertToString(json, "id"));

            if (BuiltInRegistries.BLOCK.containsKey(id))
            {
                states.addAll(BuiltInRegistries.BLOCK.get(id).getStateDefinition().getPossibleStates());
            }
            else
            {
                log.debug("Invalid block entry (not a registered block): {}", id);
            }
        }
        catch (ResourceLocationException e)
        {
            log.debug("Invalid block entry (invalid id): {}", json);
        }
    }

    private static <T extends Comparable<T>> BlockState
    readBlockStateEntry(String stringState, StateDefinition<Block, BlockState> definition, ResourceLocation blockId)
    {
        if (NORMAL_BLOCK_STATE_ENTRY.equals(stringState))
            return definition.any();

        BlockState state = definition.any();

        for (String stringProperty : stringState.split(","))
        {
            String[] propertyAndValueNames = stringProperty.split("=");
            if (propertyAndValueNames.length != 2)
            {
                log.debug("Invalid '{}' state entry (not a property name and value pair): {}", blockId, stringProperty);
                return null;
            }

            String propertyName = propertyAndValueNames[0];
            //noinspection unchecked
            Property<T> property = (Property<T>)definition.getProperty(propertyName);
            if (property == null)
            {
                log.debug("Invalid '{}' state entry (unrecognized property): {}", blockId, propertyName);
                return null;
            }

            String valueName = propertyAndValueNames[1];
            T value = property.getValue(valueName).orElse(null);
            if (value == null)
            {
                log.debug("Invalid '{}' state entry (unrecognized '{}' value): {}", blockId, propertyName, valueName);
                return null;
            }

            state = state.setValue(property, value);
        }

        return state;
    }
}
