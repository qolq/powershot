package com.gitlab.qolq.powershot;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.tree.CommandNode;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.network.chat.Component;

public final class ReloadCommand
{
    public static void register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        dispatcher.getRoot().addChild(create());
    }

    public static CommandNode<CommandSourceStack> create()
    {
        return Commands.literal(Powershot.MOD_ID)
                       .requires(source->source.hasPermission(4))
                       .then(Commands.literal("reload").executes(ReloadCommand::reloadConfigs))
                       .build();
    }

    private static int reloadConfigs(CommandContext<CommandSourceStack> context)
    {
        Powershot.loadConfigs(true);
        context.getSource().sendSuccess(()->Component.literal("Powershot configs reloaded"), true);
        return 0;
    }
}
