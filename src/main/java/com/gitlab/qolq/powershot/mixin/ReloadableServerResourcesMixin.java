package com.gitlab.qolq.powershot.mixin;

import com.gitlab.qolq.powershot.Powershot;
import net.minecraft.server.ReloadableServerResources;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ReloadableServerResources.class)
@SuppressWarnings("unused")
public final class ReloadableServerResourcesMixin
{
    @Inject(method = "updateRegistryTags(Lnet/minecraft/core/RegistryAccess;)V", at = @At("TAIL"))
    private void updateTags(CallbackInfo info)
    {
        Powershot.reloadBreakableBlocksTags();
    }
}
