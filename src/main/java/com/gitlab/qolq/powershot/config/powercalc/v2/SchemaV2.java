package com.gitlab.qolq.powershot.config.powercalc.v2;

import java.util.Map;

import com.gitlab.qolq.powershot.config.powercalc.PowerCalculationConfig;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import it.unimi.dsi.fastutil.objects.Object2FloatMap;
import it.unimi.dsi.fastutil.objects.Object2FloatMaps;
import it.unimi.dsi.fastutil.objects.Object2FloatOpenHashMap;
import net.minecraft.ResourceLocationException;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.GsonHelper;

import static com.gitlab.qolq.powershot.Powershot.log;
import static com.gitlab.qolq.powershot.config.powercalc.PowerCalculationConfig.SCHEMA_VERSION_KEY;

public final class SchemaV2
{
    private static final String BASE_POWER_KEY = "base_power";
    private static final String ENCHANTMENT_MULTIPLIERS_KEY = "enchantment_multipliers";

    public static PowerCalculationConfig read(JsonObject json)
    {
        float basePower = PowerCalculationConfig.DEFAULT_BASE_POWER;
        Object2FloatMap<ResourceLocation> enchantmentToMultiplier = Object2FloatMaps.emptyMap();

        for (Map.Entry<String, JsonElement> entry : json.entrySet())
        {
            try
            {
                switch (entry.getKey())
                {
                    case SCHEMA_VERSION_KEY -> {}
                    case BASE_POWER_KEY -> basePower = GsonHelper.convertToFloat(entry.getValue(), BASE_POWER_KEY);
                    case ENCHANTMENT_MULTIPLIERS_KEY -> enchantmentToMultiplier = readEnchantmentMultipliers(
                        GsonHelper.convertToJsonObject(entry.getValue(), ENCHANTMENT_MULTIPLIERS_KEY));
                    default -> log.debug("Ignoring unrecognized power calculation config property: {}", entry.getKey());
                }
            }
            catch (JsonParseException e)
            {
                log.debug("Failed to read power calculation config property: " + entry.getKey(), e);
            }
        }

        return new PowerCalculationConfig(basePower, enchantmentToMultiplier);
    }

    private static Object2FloatMap<ResourceLocation> readEnchantmentMultipliers(JsonObject json)
    {
        Object2FloatMap<ResourceLocation> map = new Object2FloatOpenHashMap<>();

        for (Map.Entry<String, JsonElement> entry : json.entrySet())
        {
            try
            {
                ResourceLocation id = new ResourceLocation(entry.getKey());

                if (BuiltInRegistries.ENCHANTMENT.containsKey(id))
                {
                    float multiplier = GsonHelper.convertToFloat(entry.getValue(), id.toString());
                    if (multiplier != 0.0f && Float.isFinite(multiplier))
                        map.put(id, multiplier);
                }
                else
                {
                    log.debug("Invalid enchantment multiplier entry (not a registered enchantment): '{}'", id);
                }
            }
            catch (JsonParseException | ResourceLocationException e)
            {
                log.debug("Failed to read enchantment multiplier entry: " + entry.getKey(), e);
            }
        }

        return map.isEmpty() ? Object2FloatMaps.emptyMap() : map;
    }
}
