package com.gitlab.qolq.powershot;

public interface Powerable
{
    float powershot$getPower();

    void powershot$setPower(float power);
}
