package com.gitlab.qolq.powershot;

import java.nio.file.Path;
import java.util.function.BiFunction;
import java.util.function.Function;

import com.gitlab.qolq.powershot.config.breakableblocks.BreakableBlocksConfig;
import com.gitlab.qolq.powershot.config.powercalc.PowerCalculationConfig;
import com.gitlab.qolq.powershot.mixin.ClipContextAccessor;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;
import net.fabricmc.fabric.api.event.player.PlayerBlockBreakEvents;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.AbstractArrow;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Powershot implements ModInitializer
{
    public static final String MOD_ID = "powershot";

    public static final Logger log = LogManager.getLogger(MOD_ID);

    private static BreakableBlocksConfig breakableBlocksConfig;
    private static PowerCalculationConfig powerCalculationConfig;

    @Override
    public void onInitialize()
    {
        loadConfigs(false);
        CommandRegistrationCallback.EVENT.register((dispatcher, ctx, env)->ReloadCommand.register(dispatcher));
    }

    public static void loadConfigs(boolean reloadTags)
    {
        Path dir = FabricLoader.getInstance().getConfigDir().resolve(MOD_ID);
        log.debug("Loading configs from {}", dir);

        breakableBlocksConfig = BreakableBlocksConfig.from(dir.resolve("breakable_blocks.json").toFile());
        powerCalculationConfig = PowerCalculationConfig.from(dir.resolve("power_calculation.json").toFile());

        if (reloadTags)
            breakableBlocksConfig.reloadTags();
    }

    public static void reloadBreakableBlocksTags()
    {
        breakableBlocksConfig.reloadTags();
    }

    public static float calculatePower(AbstractArrow arrow, float speed)
    {
        float power = powerCalculationConfig.basePower() * speed;
        Entity shooter = arrow.getOwner();

        if (shooter instanceof LivingEntity livingShooter)
        {
            // getUseItem() will not work for crossbows
            ItemStack stack = livingShooter.getItemInHand(livingShooter.getUsedItemHand());

            if (!stack.isEmpty())
            {
                ListTag enchantments = stack.getEnchantmentTags();

                for (int i = 0; i < enchantments.size(); i++)
                {
                    CompoundTag enchantment = enchantments.getCompound(i);
                    ResourceLocation id = ResourceLocation.tryParse(enchantment.getString("id"));

                    if (id != null && powerCalculationConfig.enchantmentToMultiplier().containsKey(id))
                    {
                        power += Mth.clamp(enchantment.getInt("lvl"), 0, 255) *
                                 powerCalculationConfig.enchantmentToMultiplier().getFloat(id);
                    }
                }
            }
        }

        return power;
    }

    public static BlockHitResult clip(Level level, ClipContext context, AbstractArrow arrow)
    {
        if (level.isClientSide)
            return level.clip(context);

        Powerable powerable = (Powerable)arrow;

        BiFunction<ClipContext, BlockPos, BlockHitResult> clipFn = (clipContext, position)->
        {
            Vec3 start = clipContext.getFrom();
            Vec3 end = clipContext.getTo();
            CollisionContext collisionContext = ((ClipContextAccessor)clipContext).getCollisionContext();
            BlockState state = level.getBlockState(position);
            VoxelShape shape = state.getCollisionShape(level, position, collisionContext);
            BreakableBlocksConfig.Entry config = breakableBlocksConfig.get(state);

            BlockHitResult result;

            if (config == null)
            {
                result = level.clipWithInteractionOverride(start, end, position, shape, state);
            }
            else
            {
                boolean breakable = powerable.powershot$getPower() >= config.powerRequirement();

                if (breakable && shape.isEmpty()) // for blocks with no collision shapes (e.g. torches)
                    shape = state.getShape(level, position, collisionContext);

                result = level.clipWithInteractionOverride(start, end, position, shape, state);
                Entity shooter = arrow.getOwner();
                BlockEntity blockEntity = level.getBlockEntity(position);

                if (breakable && result != null && preBlockBreakEvent(level, shooter, position, state, blockEntity))
                {
                    level.destroyBlock(position, true, shooter); // destroy block + do drops
                    postBlockBreakEvent(level, shooter, position, state, blockEntity);
                    powerable.powershot$setPower(powerable.powershot$getPower() - config.powerReduction());
                    result = null;
                }
            }

            if (result != null)
                powerable.powershot$setPower(0.0f);

            return result;
        };

        Function<ClipContext, BlockHitResult> missFn = clipContext->
        {
            Vec3 start = clipContext.getFrom();
            Vec3 end = clipContext.getTo();
            Vec3 dir = start.subtract(end);
            return BlockHitResult.miss(end, Direction.getNearest(dir.x, dir.y, dir.z), BlockPos.containing(end));
        };

        return BlockGetter.traverseBlocks(context.getFrom(), context.getTo(), context, clipFn, missFn);
    }

    private static boolean
    preBlockBreakEvent(Level level, Entity entity, BlockPos position, BlockState state, BlockEntity blockEntity)
    {
        if (!(entity instanceof Player player))
            return true;

        if (PlayerBlockBreakEvents.BEFORE.invoker().beforeBlockBreak(level, player, position, state, blockEntity))
            return true;

        PlayerBlockBreakEvents.CANCELED.invoker().onBlockBreakCanceled(level, player, position, state, blockEntity);
        return false;
    }

    private static void
    postBlockBreakEvent(Level level, Entity entity, BlockPos position, BlockState state, BlockEntity blockEntity)
    {
        if (entity instanceof Player)
            PlayerBlockBreakEvents.AFTER.invoker().afterBlockBreak(level, (Player)entity, position, state, blockEntity);
    }
}
