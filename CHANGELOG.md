# Change Log
All notable changes to this project will be documented in this file.

## [1.4.0-1.20.2-fabric] - 2023-10-17
### Changed
- Updated to Minecraft 1.20.2
- Updated to Fabric Loader v0.14.22
- Updated to Fabric API v0.89.3+1.20.2
  - Fabric Command API v2.2.15
  - Fabric Events Interaction v0.6.9

## [1.4.0-1.20.1-fabric] - 2023-06-19
### Changed
- Updated to Minecraft 1.20.1
- Updated to Fabric Loader v0.14.21
- Updated to Fabric API v0.83.1+1.20.1
  - Fabric Command API v2.2.11
  - Fabric Events Interaction v0.6.0

### Added
- Default breakable block config block entry
    - `minecraft:decorated_pot`

## [1.4.0-1.19.4-fabric] - 2023-03-15
### Changed
- Updated to Minecraft 1.19.4
- Updated to Fabric Loader v0.14.17
- Updated to Fabric API v0.75.3+1.19.4
  - Fabric Command API v2.2.4
  - Fabric Events Interaction v0.4.42

## [1.4.0-1.19.3-fabric] - 2023-01-05
### Changed
- Updated to Minecraft 1.19.3
- Updated to Fabric Loader v0.14.11
- Updated to Fabric API v0.69.1+1.19.3
  - Fabric Command API v2.1.15
  - Fabric Events Interaction v0.4.37

## [1.4.0-1.19-fabric] - 2022-06-08
### Changed
- Updated to Minecraft 1.19

## [1.4.0-1.18.2-fabric] - 2022-03-09
### Changed
- Updated to Minecraft 1.18.2

## [1.4.0-1.18-fabric] - 2021-12-04
### Changed
- Updated to Minecraft 1.18

## [1.4.0-1.17-fabric] - 2021-11-22
### Added
- Support for Fabric API player block break events

## [1.3.0-1.17-fabric] - 2021-11-08
### Fixed
- Config not reading block entries correctly

### Added
- `powershot reload` command to reload configs in-game

## [1.2.0-1.17-fabric] - 2021-06-09
### Changed
- Updated to Minecraft 1.17

### Added
- Default breakable block config entries for
  - Blocks
    - `minecraft:lightning_rod`
    - `minecraft:pointed_dripstone`
    - `minecraft:small_amethyst_bud`
    - `minecraft:medium_amethyst_bud`
    - `minecraft:large_amethyst_bud`
    - `minecraft:amethyst_cluster`
    - `minecraft:azalea`
    - `minecraft:big_dripleaf`
    - `minecraft:big_dripleaf_stem`
    - `minecraft:spore_blossom`
    - `minecraft:cave_vines`
    - `minecraft:cave_vines_plant`
    - `minecraft:glowstone`
    - `minecraft:melon_stem`
    - `minecraft:attached_melon_stem`
    - `minecraft:pumpkin_stem`
    - `minecraft:attached_pumpkin_stem`
    - `minecraft:turtle_egg`
  - Tags
    - `minecraft:candles`

## [1.1.1-1.16.2-fabric] - 2021-01-24
### Fixed
- `NoSuchMethodError` crashes on dedicated server

## [1.1.0-1.16.2-fabric] - 2020-08-12
### Changed
- Updated to Minecraft 1.16.2

## [1.1.0-1.16.1-fabric] - 2020-07-29
### Fixed
- Occasional crashing when config files are invalid

### Changed
- Config file formats

### Added
- Option to specify multipliers for arbitrary enchantments in config
- Default breakable block config entries for
  - `minecraft:soul_lantern`
  - `minecraft:soul_torch`
  - `minecraft:soul_wall_torch`

## [1.0.0-1.16.1-fabric] - 2020-07-08
### Changed
- Updated to Minecraft 1.16.1

## [1.0.0-1.15.2-fabric] - 2020-07-06
### Fixed
- Breakable blocks config _object block entries_ being serialized as
  _top-level entries_
- Crashing when breakable blocks config has invalid entries

### Changed
- Config file name from `general.json` to `power_calculation.json`
- Breakable blocks config to not accept _string top-level entries_
- Default breakable blocks config values

### Added
- Powershot logo and icon
- Block tag support for breakable block config file

## [0.1.0] - 2020-06-10
### Added
- Block-breaking behavior to arrows
  - Arrow power level which decreases with each block broken
  - Config file to specify breakable blocks and their properties
