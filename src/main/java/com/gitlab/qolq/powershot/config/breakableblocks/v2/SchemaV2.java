package com.gitlab.qolq.powershot.config.breakableblocks.v2;

import com.gitlab.qolq.powershot.config.breakableblocks.BreakableBlocksConfig;
import com.gitlab.qolq.powershot.config.breakableblocks.v1.SchemaV1;
import com.google.gson.JsonObject;
import net.minecraft.util.GsonHelper;

import static com.gitlab.qolq.powershot.Powershot.log;

public final class SchemaV2
{
    private static final String ENTRIES_KEY = "entries";

    public static BreakableBlocksConfig read(JsonObject json)
    {
        if (json.has(ENTRIES_KEY))
        {
            return SchemaV1.read(GsonHelper.getAsJsonArray(json, ENTRIES_KEY));
        }
        else
        {
            log.debug("Breakable blocks config has no entries");
            return new BreakableBlocksConfig();
        }
    }
}
